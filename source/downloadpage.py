from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
import json
import sys

#   $uator->proxy([qw/ http https /] => 'socks://localhost:9050'); # Tor proxy

capabilities = webdriver.DesiredCapabilities.CHROME

option = Options()
option.add_argument('--headless')
option.add_argument('--no-sandbox')
option.add_argument('--disable-gpu')
option.add_argument('--disable-dev-sh-usage')

prefs = {"profile.managed_default_content_settings.images": 2}
option.add_experimental_option("prefs", prefs)
#option.add_argument("user-agent=Mozilla/5.0 (Windows Phone 10.0; Android 4.2.1; Microsoft; Lumia 640 XL LTE) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Mobile Safari/537.36 Edge/12.10166")
option.add_argument("user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.1 Safari/605.1.15")
service = Service('/usr/bin/chromedriver')
driver = webdriver.Chrome(service=service, options=option) #, desired_capabilities=capabilities)
#driver.implicitly_wait(60) 
url = sys.argv[1]
filename = sys.argv[2]

print("Getting: " + url)
driver.get(url) # Getting page HTML through request

#code = get_status(driver.get_log('performance'))
#print("Code: " + str(code))

with open(filename, 'w') as file:
    file.write(driver.page_source)


driver.quit()
