# What is emptyproject?

Emptyproject is a template project structure for development projects running on a Multipass provided VM using the scripts provided by https://bitbucket.org/richard5/multipasssetup

## Usage:
It uses separate directories for source code, data, scheduling and shell scripts. Some will provided as an example.

- data: this is the folder in which configuration and logfiles should end up.
- schedule: this is where the cron information is stored
- scripts: the location for all shell scripts to update, install or whatever maintenance you require.
- source: the actual source code for the python or perl programs yuo want to run.


## Schedule format
The format of the scheduling is very crontab like, see the included example:

`5 10 * * * $PROJECTDIR/$PROJECTNAME/scripts/test.sh >>$PROJECTDIR/$PROJECTNAME/data/test.log`

You can use environment variables to help you determine locations of scripts and/or logfiles which are defined in the multipasssetup scripts.

## Scripts
The are some scripts provided, namely for setting up the scheduled tasks using crontab, an update script and an init script.

The update script is there to run daily as the first script to pull any changes from you git repository and make sure you run the latest version of everything. It runs the crontab script at the end. If there is the need for restarting any continous script or program include it here. The scripts contains a check on the hostname, the hostname needs to start with prod to update everything. There is no need to run this on development machines. 

The crontab script runs daily usually right after the update script, first it removes all project related entries and then copies the one from ./schedule/crontab.txt into the crontab where it will replace the environment variables. It will only do this on the server where hostname starts with prod.

The init script is only used during the first setup. It's called by the initproject script from multipasssetup.