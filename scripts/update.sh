#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
echo "test script"
date
TEMP=`realpath "$BASH_SOURCE"`
MYPATH=`dirname "$TEMP"`
IFS='/' read -ra my_array <<< "$MYPATH"
#echo ${#my_array[@]}
PROJECTNAME=${my_array[ ${#my_array[@]} - 2 ]}
echo projectname $PROJECTNAME
export PROJECTNAME=$PROJECTNAME
COUNT=1
while [ "${my_array[ $COUNT ]}" != "$PROJECTNAME" ]
do
   PROJECTDIR=$PROJECTDIR/${my_array[ $COUNT ]}
   COUNT=$COUNT+1
done

cd /home/ubuntu/projects/$PROJECTNAME

# make sure that keyagent will work properly for git pull
. ~/.keychain/$HOSTNAME-sh

# only production environments should pull from git not development, hostname should indicate environment
if [[ $HOSTNAME == prod* ]] ;
then
   echo "Production environment, git pull."
   git pull origin master
fi
$PROJECTDIR/$PROJECTNAME/scripts/crontab.sh
echo "finished test"
