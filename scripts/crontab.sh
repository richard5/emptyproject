#!/bin/bash
export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin
echo "Refreshing crontab script"

echo datetime
TEMP=`realpath "$BASH_SOURCE"`
#echo `dirname "$TEMP"`
MYPATH=`dirname "$TEMP"`
IFS='/' read -ra my_array <<< "$MYPATH"
PROJECTNAME=${my_array[ ${#my_array[@]} - 2 ]}
export PROJECTNAME=$PROJECTNAME
COUNT=1
PROJECTDIR=""
while [ "${my_array[ $COUNT ]}" != "$PROJECTNAME" ]
do
#  echo ${my_array[ $COUNT ]}
   PROJECTDIR=$PROJECTDIR/${my_array[ $COUNT ]}
#   echo $PROJECTDIR
   COUNT=$COUNT+1
done

export PROJECTDIR=$PROJECTDIR
crontab -l | grep -v "$PROJECTNAME" | crontab -
(crontab -l; envsubst < "$PROJECTDIR/$PROJECTNAME/schedule/crontab.txt")|awk '!x[$0]++'|crontab -
echo "crontab script finished"
